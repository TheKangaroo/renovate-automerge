module.exports = {
  platform: "gitlab",
  token: process.env.RENOVATE_TOKEN,
  automergeType: "pr",
  platformAutomerge: true,
  gitLabIgnoreApprovals: true,
  onboarding: true,
  enabledManagers: ["gitlabci"],
  extends: [
    // "config:base", - those that seem to make sense to me
    ":semanticPrefixFixDepsChoreOthers",
    ":ignoreModulesAndTests",
    ":autodetectRangeStrategy",
    "group:monorepos",
    "group:recommended",
    "workarounds:all",
    // end "config:base",
    ":prHourlyLimitNone",
    ":prConcurrentLimitNone",
    ":label(renovate)",
    // add docker digests if missing
    "docker:pinDigests",
  ],
  packageRules: [
    // automerge config
    {
      groupName: "automergeable deps",
      matchUpdateTypes: ["digest"],
      automerge: true,
    },
  ],
  repositories: ["TheKangaroo/renovate-automerge"],
};
